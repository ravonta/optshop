'use strict';
module.exports = (sequelize, DataTypes) => {
  const consignment = sequelize.define('consignment', {
    quantity: DataTypes.INTEGER,
    deadline: DataTypes.DATE
  }, {});
  consignment.associate = function(models) {
    models.Consignment.hasOne(models.Product);
  };
  return consignment;
};