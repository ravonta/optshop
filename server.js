const TelegramBot = require('node-telegram-bot-api');
const Models = require('./models')
var shttps = require('socks5-https-client/lib/Agent');
var async = require('async');


const token = '945344730:AAFwu7zcg1CMvvPDE9ubotT_m8ubeADvMy0';

const bot = new TelegramBot(token, {polling: true,
/*    request: {
        agentClass: shttps,
        agentOptions: {
            socksHost: "139.59.209.179",
            socksPort: parseInt("1080"),
            // If authorization is needed:
            // socksUsername: process.env.PROXY_SOCKS5_USERNAME,
            // socksPassword: process.env.PROXY_SOCKS5_PASSWORD
        }
    }*/
});

var menu = {
    reply_markup: {
        inline_keyboard: [
            [{ text: 'Категории товаров', callback_data: JSON.stringify({"type": "categories"}) },
            { text: 'Популярные товары', callback_data: JSON.stringify({"type": "submenu", "submenu": "menu"}) }]
        ]
    }
};


bot.onText(/\/start/, function (msg, match) {
    bot.sendMessage(msg.chat.id, 'OPTSHOP', menu);
});

bot.on('callback_query', (callback)=>{
    let data = JSON.parse(callback.data);
    bot.deleteMessage(callback.from.id, callback.message.message_id);

    switch (data.type) {
        case 'categories':
            async.waterfall([
                (cb) => {
                    Models.Category.findAll({attributes: ['id', 'name'], raw: true})
                        .then((categories)=>{cb(null, categories)});
                },
                (categories, cb) => {
                    let inline_keyboard = [];
                    inline_keyboard[0] = [];
                    categories.forEach((category, i, categories)=>{
                        inline_keyboard[0].push({
                            text: category['name'],
                            callback_data: JSON.stringify({"type": "ctg", "categoryId": category.id, "offset": 0})
                        });
                    });
                    inline_keyboard[1] = [{
                        text: "В главное меню...",
                        callback_data: JSON.stringify({"type": "main"})
                    }]
                    cb(null, inline_keyboard);
                }
            ], (err, inline_keyboard) => {
                bot.sendMessage(callback.from.id, 'Выберите категорию:', {reply_markup: {inline_keyboard: inline_keyboard}});
            });
            break;
        case 'ctg':
            async.waterfall([
                (cb) => {
                    Models.Product.findAll({where: {categoryId: data.categoryId}, offset: data.offset, limit: 1, raw: true})
                        .then((product)=>{cb(null, product[0])});
                },
                (product, cb)=>{
                    Models.Product.count({where: {categoryId: data.categoryId}}).then((countProducts)=>{
                        cb(null, product, countProducts)
                    })
                },
                (product, countProducts, cb)=>{
                    let inline_keyboard = [];
                    inline_keyboard[0] = [];
                    inline_keyboard[1] = [];

                    if (data.offset > 0){
                        inline_keyboard[0].push({
                            text: 'Предыдущий товар',
                            callback_data: JSON.stringify({"type": "ctg", "categoryId": data.categoryId,  "offset": data.offset-1})
                        });
                    }

                    if (data.offset+1 < countProducts){
                        inline_keyboard[0].push({
                            text: 'Следующий товар',
                            callback_data: JSON.stringify({"type": "ctg", "categoryId": data.categoryId, "offset": data.offset+1})
                        });
                    }

                    inline_keyboard[1].push({
                        text: 'Назад в категории...',
                        callback_data: JSON.stringify({"type": "categories"})
                    });

                    inline_keyboard[1].push({
                        text: 'В главное меню...',
                        callback_data: JSON.stringify({"type": "main"})
                    });

                    cb(null, product, inline_keyboard)

                }
            ], (err, product, inline_keyboard)=>{
                let text = 'Наименование: '+product.name+'\n'+
                    'Описание: '+product.description+'\n'+
                    'Стоимость: '+product.cost;
                bot.sendMessage(callback.from.id, text, {reply_markup: {inline_keyboard: inline_keyboard}});
            });
            break;

        case 'main':
            bot.sendMessage(callback.from.id, 'OPTSHOP', menu);
            break;
    }
});