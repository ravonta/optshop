'use strict';

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('categories', [
      {name: 'Наушники', createdAt: new Date(), updatedAt: new Date()},
      {name: 'Часы', createdAt: new Date(), updatedAt: new Date()}
    ], {});

    const categories = await queryInterface.sequelize.query(
        `SELECT id from categories;`
    );

    const categoriesRows = categories[0];

    return await queryInterface.bulkInsert('products', [
      {name: 'Apple', description: 'Понты дороже денег', categoryId: categoriesRows[0].id, cost: 200, createdAt: new Date(), updatedAt: new Date()},
      {name: 'Samsung', description: 'Понты, которые стоят немного дешевле', categoryId: categoriesRows[0].id, cost: 150, createdAt: new Date(), updatedAt: new Date()},
      {name: 'Rolex', description: 'Что они здесь делают?', categoryId: categoriesRows[0].id, cost: 500, createdAt: new Date(), updatedAt: new Date()},
      {name: 'Apple', description: 'Они даже показывают время', categoryId: categoriesRows[1].id, cost: 200, createdAt: new Date(), updatedAt: new Date()},
      {name: 'Samsung', description: 'У них тоже есть часы?', categoryId: categoriesRows[1].id, cost: 150, createdAt: new Date(), updatedAt: new Date()},
      {name: 'Rolex', description: 'Вот, что называется часами', categoryId: categoriesRows[1].id, cost: 500, createdAt: new Date(), updatedAt: new Date()},
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('categories', null, {});
    await queryInterface.bulkDelete('products', null, {});
  }
};
